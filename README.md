### Link
https://kiratech-assessment.surge.sh/

### Technical Considerations
- For some reason, my laptop unable to run the sample bootstrapped project at https://bitbucket.org/kiratechnologies/fe-assignment-bootstrap/src/master, so decided to create new Vite project instead
- Decided to use Quasar framework, as it simplifies many things (table sorting, searching/filtering, pagination, etc...)
